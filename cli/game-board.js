"use strict";

require("colors");
const overlap = require("overlap");
const sprintf = require("sprintf-js").sprintf;
const boxen = require("boxen");

module.exports =GameBoard;

const defaults = {
    columnMax: 8,
    columnWidth: 12,
    cardHeight:8
};
function GameBoard(player1, player2, common, options) {
    if(!options) {
        options = defaults;
    }
	
    this.toString = function() {
		let result="";
		if(player1.isActive()) {
		    result+=selection(player1.cards.length);	
		}
        result+=cards(player1.cards);
        result+=life(player1.life);
        result+=resorses(player1.resources);
        result+=resorses(common.resources);
        result+=resorses(player2.resources);
        result+=life(player2.life);
        result+=cards(player2.cards);
        if(player2.isActive()) {
		    result+=selection(player2.cards.length);	
		}
        return boxen(result);
    };
	
    const life = function(numberOfLife) {
        return " ".repeat(options.columnWidth+2)+"♥ ".repeat(numberOfLife).red+"\n";
    };
    
    const selection = function(numberOfCards) {
        let result="  ";
        for(let i=0; i < numberOfCards;i++) {
            result+=(i+1).toString().green+" ".repeat(options.columnWidth-1);
	    }
        return result+"\n";
    };

    const cards = function(cardsOfPlayer) {
        let result="\n".repeat(options.cardHeight);
        let count =0;
        for(let c of cardsOfPlayer) {
            result=line(result, card(c),count,options.columnWidth);
            count++;
        }
        return result;
    };

    const resorses = function(gameResurs) {
        let proberties = Object.keys(gameResurs);
        let result="";
        let column="";
        let column_number=0;
        for(let i = 0;  i < proberties.length; i++) {
            column+=sprintf("%2s %2s\n", proberties[i], gameResurs[proberties[i]]);
            if(i%4===3) {
                result=line(result,column,column_number,options.columnWidth);
                column="";
                column_number++;
            }
        }
        result=line(result,column,column_number,options.columnWidth);
        result+=" ".repeat(options.columnWidth*options.columnMax-2);
        return boxen(result)+"\n";
    };

    const line = function(result, column, columnNumber, columnWidth) {
        return overlap({
            who: result,
            with: column,
            where: {
                x: options.columnWidth*columnNumber,
                y: 0
            }
        });
    };

    const card = function(cardOfPlayer) {
        let result="";
        for(let action of cardOfPlayer.actions) {
            result+=sprintf("%3s %2s %1s%2s\n", action[0], action[1], action[2], action[3]);
        }
        result+="          \n".repeat(5-cardOfPlayer.actions.length);
        return boxen(result);
    };
}

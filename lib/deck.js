"use strict";

module.exports = Deck;

function Deck(deck, collection) {
    const cards=deck.cards;
	
    this.draw = function() {
        let index=Math.floor(Math.random()*(cards.length-1));
        let card = cards[index];
        cards.splice(index,1);
        return card;
    };
		
}

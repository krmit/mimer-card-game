"use strict";

const Player = require("./player.js");
const fetch = require("isomorphic-fetch");

module.exports = Game;

const defaults = {
	
};

function readJson(urlToJson) {
    return fetch(urlToJson).then((response) => {return response.json();});
}

function Game(options) {
    if(!options) {
        options = defaults;
    }
	
    this.player1=null;
    this.player2=null;
    this.common={};
	
    this.start = Promise.all([readJson(options.collection), 
        readJson(options.player1.deck), 
        readJson(options.player2.deck)])
        .then( files => {
            this.player1=new Player(files[1], files[0]);
            this.player2=new Player(files[2], files[0]);
        });
}

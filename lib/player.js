"use strict";

const Deck = require("./deck.js");

module.exports = Player;

function Player(deck,collection) {
    this.resources={};
    this.cards=[];
    this.life=5;
    this.deck = new Deck(deck,collection);
	
    this.draw = function(cardIndex) {
        this.cards[cardIndex]=this.deck.draw();	
    };
	
}

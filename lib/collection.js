"use strict";

const fs = require("fs");
const yaml = require("js-yaml");

module.exports = Collection;

function Collection(collectionName) {
    const cards_data=yaml.safeLoad(fs.readFileSync("../collections/"+collectionName+".yml"));
    return cards_data.cards;
}
